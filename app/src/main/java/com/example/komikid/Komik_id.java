package com.example.komikid;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Komik_id extends AppCompatActivity{

    class Judul{
        private String name,keterangan,imageURL;

        public Judul(String name, String keterangan, String imageURL){
            this.name = name;
            this.keterangan = keterangan;
            this.imageURL = imageURL;
        }
        public String getName(){
            return name;
        }
        public String getKeterangan(){
            return keterangan;
        }
        public String getImageURL(){
            return imageURL;
        }
    }

    public class GridViewAdapter extends BaseAdapter{
        Context c;
        ArrayList<Judul> juduls;

        public GridViewAdapter(Context c, ArrayList<Judul> juduls){
            this.c = c;
            this.juduls = juduls;
        }

        @Override
        public int getCount() {
            return juduls.size();
        }

        @Override
        public Object getItem(int position) {
            return juduls.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if(view == null){
                view = LayoutInflater.from(c).inflate(R.layout.row_model,parent,false);
            }

            TextView txtName = view.findViewById(R.id.nameTV);
            TextView txtDesc = view.findViewById(R.id.desTV);
            ImageView komikImgView = view.findViewById(R.id.img_card);

            final Judul judul = (Judul)this.getItem(position);

            txtName.setText(judul.getName());
            txtDesc.setText(judul.getKeterangan());

            if (judul.getImageURL() != null && judul.getImageURL().length() > 0){
                Picasso.get().load(judul.getImageURL()).placeholder(R.drawable.ic_home_white_24dp).into(komikImgView);
            }else{
                Toast.makeText(c, "Empty image URL", Toast.LENGTH_LONG).show();
                Picasso.get().load(R.drawable.ic_home_white_24dp).into(komikImgView);
            }
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(c, judul.getName(),Toast.LENGTH_LONG).show();
                }
            });
            return view;
        }
    }

    public class DataRetiever{
        private static final String PHP_MYSQL_SITE_URL = "http://192.168.1.5/database/image/";
        //private static final String PHP_MYSQL_SITE_URL = "http://192.168.1.4/database/image/";
        //private static final String PHP_MYSQL_SITE_URL = "http://192.168.43.240/database/image/";

        private final Context c;
        private GridViewAdapter adapter;

        public  DataRetiever(Context c){
            this.c = c;
        }

        public void retrieve(final GridView gv, final ProgressBar myProgressBar){
            final ArrayList<Judul> juduls = new ArrayList<>();

            myProgressBar.setIndeterminate(true);
            myProgressBar.setVisibility(View.VISIBLE);

            AndroidNetworking.get(PHP_MYSQL_SITE_URL)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            JSONObject jo;
                            Judul judul;
                            try {
                                for (int i=0; i<response.length();i++){
                                    jo = response.getJSONObject(i);

                                    int id = jo.getInt("id_img");
                                    String nama = jo.getString("judul");
                                    String keterangan = jo.getString("keterangan");
                                    String background = jo.getString("background");

                                    judul = new Judul(nama,keterangan,PHP_MYSQL_SITE_URL+"/img_data/"+background);
                                    juduls.add(judul);
                                }

                                adapter = new GridViewAdapter(c,juduls);
                                gv.setAdapter(adapter);
                                myProgressBar.setVisibility(View.GONE);

                            }catch (JSONException e){
                                myProgressBar.setVisibility(View.GONE);
                                Toast.makeText(c,"GOOD RESPONSE but Java Can't parse JSON "+e.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.printStackTrace();
                            myProgressBar.setVisibility(View.GONE);
                            Toast.makeText(c, "Unseccessful : Error is : "+anError.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_komik_id);

        GridView myGridView = findViewById(R.id.myGridView);
        ProgressBar myDataLoaderProgressBar = findViewById(R.id.myDataLoaderProgressBar);

        new DataRetiever(Komik_id.this).retrieve(myGridView,myDataLoaderProgressBar);
    }
}
