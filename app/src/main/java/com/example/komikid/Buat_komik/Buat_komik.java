package com.example.komikid.Buat_komik;

public class Buat_komik {
    private int id;
    private String jdulCvr;
    private byte[] img;

    public Buat_komik(String jdulCvr, byte[] img, int id){
        this.jdulCvr = jdulCvr;
        this.img = img;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJdulCvr() {
        return jdulCvr;
    }

    public void setJdulCvr(String name) {
        this.jdulCvr= jdulCvr;
    }

    public byte[] getImage() {
        return img;
    }

    public void setImage(byte[] img){
        this.img= img;
    }
}
