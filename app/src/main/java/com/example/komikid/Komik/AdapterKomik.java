package com.example.komikid.Komik;

public class AdapterKomik {
    private String id_eps;
    private String ket_eps;
    private String jdl_eps;

    public AdapterKomik(String id_eps, String jdl_eps, String ket_eps) {
        this.id_eps = id_eps;
        this.jdl_eps = jdl_eps;
        this.ket_eps = ket_eps;
    }

    public String getId_eps() {
        return id_eps;
    }

    public String getKet_eps() {
        return ket_eps;
    }

    public String getJdl_eps() {
        return jdl_eps;
    }
}
