package com.example.komikid.Episode;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

public class DBHelper {
    private SQLiteDatabase sqlDB;
    private static final int DBVersion = 1;
    private static final String DBName = "komik_id";
    private static final String TName = "Episode";
    static final String id = "id";
    static final String ket = "keterangan";
    static final String jdl_kmk = "judul_komik";

    private static final String TName1 = "Komik";
    static final String id_eps = "id_eps";
    static final String ket_eps = "keterangan_eps";
    static final String jdl_eps = "judul_eps";

    private static final String TName2 = "Episode_komik";
    static final String id_EK = "id_EpsKmk";
    static final String ket_EK = "keterangan_EpsKmk";
    static final String jdl_EK = "judul_EpsKmk";

    private static final String BuatEpisode = "Create table IF NOT EXISTS " + TName +
            "(id integer PRIMARY KEY AUTOINCREMENT,"+ jdl_kmk +
            " text," + ket + " text);";

    private static final String BuatKomik = "Create table IF NOT EXISTS " + TName1 +
            "(id_eps integer PRIMARY KEY AUTOINCREMENT,"+ jdl_eps +
            " text," + ket_eps + " text);";

    private static final String BuatEpsKmk = "Create table IF NOT EXISTS " + TName2 +
            "(id_EK integer PRIMARY KEY AUTOINCREMENT,"+ jdl_EK +
            " text," + ket_EK + " text);";

    static class DatabaseHelperUser extends SQLiteOpenHelper {

        Context context;
        DatabaseHelperUser(Context context) {
            super(context,DBName,null,DBVersion);
            this.context=context;
        }

        @Override
        public void onCreate(SQLiteDatabase db){
            db.execSQL(BuatKomik);
            db.execSQL(BuatEpisode);
            db.execSQL(BuatEpsKmk);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("Drop table IF EXISTS "+ TName);
            db.execSQL("Drop table IF EXISTS "+ TName1);
            db.execSQL("Drop table IF EXISTS "+ TName2);
            onCreate(db);
        }
    }

    DBHelper(Context context) {
        DatabaseHelperUser db = new DatabaseHelperUser(context);
        sqlDB=db.getWritableDatabase();
    }

    public void InsertEps(ContentValues values1) {
        sqlDB.insert(TName1, "", values1);
    }

    public Cursor QueryEps(String[] Projection1, String Selection1, String[] SelectionArgs1, String SortOrder1) {
        SQLiteQueryBuilder queryBuilder1 = new SQLiteQueryBuilder();
        queryBuilder1.setTables(TName1);
        return queryBuilder1.query(sqlDB, Projection1, Selection1, SelectionArgs1,
                null, null, SortOrder1);
    }

    public int DeleteEps(String selection, String[] selectionargs) {
        return sqlDB.delete(TName1, selection, selectionargs);
    }

    public int UpdateEps(ContentValues contentValues, String selection, String[] selectionArgs) {
        return sqlDB.update(TName1, contentValues, selection, selectionArgs);
    }

    //Insert,Query,Delete,Update Episode

    public void Insert(ContentValues values) {
        sqlDB.insert(TName, "", values);
    }

    public Cursor Query(String[] Projection, String Selection, String[] SelectionArgs, String SortOrder) {

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TName);
        return queryBuilder.query(sqlDB, Projection, Selection, SelectionArgs,
                null, null, SortOrder);
    }

    public int Delete(String selection, String[] selectionargs) {
        return sqlDB.delete(TName, selection, selectionargs);
    }

    public int Update(ContentValues contentValues, String selection, String[] selectionArgs) {
        return sqlDB.update(TName, contentValues, selection, selectionArgs);
    }

    //Insert,Query,Delete,Update EpsKmk

    public void Insert2(ContentValues values2) {
        sqlDB.insert(TName2, "", values2);
    }

    public Cursor Query2(String[] Projection, String Selection, String[] SelectionArgs, String SortOrder) {

        SQLiteQueryBuilder queryBuilder2 = new SQLiteQueryBuilder();
        queryBuilder2.setTables(TName2);
        return queryBuilder2.query(sqlDB, Projection, Selection, SelectionArgs,
                null, null, SortOrder);
    }

    public int Delete2(String selection, String[] selectionargs) {
        return sqlDB.delete(TName2, selection, selectionargs);
    }

    public int Update2(ContentValues contentValues, String selection, String[] selectionArgs) {
        return sqlDB.update(TName2, contentValues, selection, selectionArgs);
    }
}