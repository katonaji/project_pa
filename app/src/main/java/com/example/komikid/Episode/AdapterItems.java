package com.example.komikid.Episode;

public class AdapterItems {
    private String id;
    private String ket;
    private String jdl_kmk;

    AdapterItems(String id, String jdl_kmk, String ket) {
        this.id = id;
        this.jdl_kmk = jdl_kmk;
        this.ket = ket;
    }

    public String getId() {
        return id;
    }

    public String getKet() {
        return ket;
    }

    public String getJdl_kmk() {
        return jdl_kmk;
    }
}
