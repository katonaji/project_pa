package com.example.komikid.Episode;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.komikid.R;

import java.io.File;
import java.util.ArrayList;

public class EpisodeKomik extends AppCompatActivity {

    DBHelper dbHelper;
    boolean b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_episode_komik);

        FloatingActionButton fab1 = findViewById(R.id.fab);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), TambahData.class));
                EpisodeKomik.this.finish();
            }
        });

        dbHelper = new DBHelper(this);
        loadTable();
    }

    ArrayList<AdapterEK> listnewsEK = new ArrayList<>();
    MyCustomAdapter myadapter;

    private void loadTable() {
        listnewsEK.clear();
        String[] projection = {DBHelper.id_EK, DBHelper.jdl_EK, DBHelper.ket_EK};
        String sortorder = DBHelper.id_EK + " DESC";
        Cursor cursor = dbHelper.Query2(projection, null, null, sortorder);
        if (cursor.moveToFirst()) {
            do {
                listnewsEK.add(new AdapterEK(cursor.getString(cursor.getColumnIndex(DBHelper.id_EK)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.jdl_EK)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.ket_EK))));
            } while (cursor.moveToNext());
        }

        myadapter = new MyCustomAdapter(listnewsEK);
        ListView lsNews = findViewById(R.id.lvEpsTampil);
        lsNews.setAdapter(myadapter);
        TextView tvKosong = findViewById(R.id.tvEpsKosong);

        if(myadapter.isEmpty()) {
            lsNews.setVisibility(View.GONE);
            tvKosong.setVisibility(View.VISIBLE);
        } else {
            lsNews.setVisibility(View.VISIBLE);
            tvKosong.setVisibility(View.GONE);
        }
    }

    private class MyCustomAdapter extends BaseAdapter {
        ArrayList<AdapterEK> listnewsEKAdpater ;

        MyCustomAdapter(ArrayList<AdapterEK> listnewsEKAdpater) {
            this.listnewsEKAdpater=listnewsEKAdpater;
        }

        @Override
        public int getCount() {
            return listnewsEKAdpater.size();
        }

        @Override
        public String getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater mInflater = getLayoutInflater();
            View myView = mInflater.inflate(R.layout.layout_ek,null);
            final AdapterEK s = listnewsEKAdpater.get(position);

            final TextView tvJdlKmk = myView.findViewById(R.id.tvmerk);
            tvJdlKmk.setText(s.getId_EK());

            ImageView imageView = myView.findViewById(R.id.ivimage);
            imageView.setVisibility(View.VISIBLE);
            File imgFile = new File(Environment.getExternalStorageDirectory().
                    getAbsolutePath() + "/komik_id/img/" + s.getJdl_EK() + " " +
                    s.getKet_EK() + ".jpg");
            if(imgFile.isFile()){
                Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                imageView.setImageBitmap(bitmap);
            } else {
                imageView.setVisibility(View.GONE);
            }

            final ListView listView = findViewById(R.id.lvEpsTampil);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String idlist = listnewsEK.get(position).getId_EK();

                    Intent intent = new Intent(getApplicationContext(), TampilEK.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("idlist", idlist);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    EpisodeKomik.this.finish();
                }
            });

            return myView;
        }
    }
}
