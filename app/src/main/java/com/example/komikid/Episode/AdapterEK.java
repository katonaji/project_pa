package com.example.komikid.Episode;

public class AdapterEK {
    private String id_EK;
    private String ket_EK;
    private String jdl_EK;

    AdapterEK(String id_EK, String jdl_EK, String ket_EK) {
        this.id_EK = id_EK;
        this.jdl_EK = jdl_EK;
        this.ket_EK = ket_EK;
    }

    public String getId_EK() {
        return id_EK;
    }

    public String getKet_EK() {
        return ket_EK;
    }

    public String getJdl_EK() {
        return jdl_EK;
    }
}
