package com.example.komikid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.komikid.Episode.MainEpisode;

public class MenuButton extends AppCompatActivity {

    Button listkmk, buat, aba, coba;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_button);

        aba = (Button) findViewById(R.id.aba);
        listkmk = (Button) findViewById(R.id.list_kmk);
        buat = (Button) findViewById(R.id.buat);
        coba = (Button) findViewById(R.id.coba);

        coba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v==coba){
                    Intent intent = new Intent(MenuButton.this, MakeGambar.class);
                    startActivity(intent);
                }
            }
        });

        buat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v==buat){
                    Intent intent = new Intent(MenuButton.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        });

        listkmk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v==listkmk){
                    Intent intent = new Intent(MenuButton.this,Komik_id.class);
                    startActivity(intent);
                }
            }
        });

        aba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == aba){
                    Intent intent = new Intent(MenuButton.this, MainEpisode.class);
                    startActivity(intent);
                }
            }
        });
    }
}